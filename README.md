# RangeList

## Algorithm

Use a sorted array to store data, and this array's length will be always even.

If range list is `[1, 5) [8, 10)`, then the array will be `[1, 5, 8, 10]`.

Based on sorted array, the search can be very fast. 

Finally, time complexity is `O(logn)`, space complexity is `O(n)`.

### Steps

When we add/remove a range, which has a `start val` and a `terminate(term) val`, the codes will do like this:

1. Find the two indexes for both `start val` and `term val`. It means the last index that `less than or equal to` the comming val.
2. Then there are 4 basic cases based on upper indexes, and process them individually.

## Cases
We list cases by the two indexes' parity.

| odd-even                 | even-even                  |even-odd                   |odd-odd                   |
| ------------------------ | -------------------------- | ------------------------- | ------------------------ |
| ![](images/odd-even.png) | ![](images/even-even.png)  | ![](images/even-odd.png)  | ![](images/odd-odd.png)  |
|                          | ![](images/even-even2.png) | ![](images/even-odd2.png) | ![](images/odd-odd2.png) |


## Sample outputs
Run `ruby sample.rb` to get sample outputs.

Maybe you have observed. There are a trick for the output: at add/remove line, the values have been replaced to the correct place.

### `RangeList#add`
```rb
current: [ 1,   8) [10,  26) [50,  90)
add:     [          10,       50)
index:2 4
result:  [ 1,   8) [10,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
add:     [               27,  50)
index:3 4
result:  [ 1,   8) [10,  26) [27,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
add:     [               26,27)
index:3 3
result:  [ 1,   8) [10,  27) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
add:     [               27,28)
index:3 3
result:  [ 1,   8) [10,  26) [27,  28) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
add:     [          12,  28)
index:2 3
result:  [ 1,   8) [10,  28) [50,  90)
----------------------------------------
```

### `RangeList#remove`
```rb
current: [ 1,   8) [10,  26) [50,  90)
remove:  [          10,       50)
index:2 4
result:  [ 1,   8) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
remove:  [          11,       50)
index:2 4
result:  [ 1,   8) [10,  11) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
remove:  [          10,  30)
index:2 3
result:  [ 1,   8) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
remove:  [          11,  30)
index:2 3
result:  [ 1,   8) [10,  11) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
remove:  [      9,       30)
index:1 3
result:  [ 1,   8) [50,  90)
----------------------------------------
current: [ 1,   8) [10,  26) [50,  90)
remove:  [      9,  11)
index:1 2
result:  [ 1,   8) [11,  26) [50,  90)
----------------------------------------
```
