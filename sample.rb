require File.expand_path("../range_list.rb", __FILE__)

rl = RangeList.new
rl.add([1, 5])
rl.print
# Should display: [1, 5)
rl.add([10, 20])
rl.print
# Should display: [1, 5) [10, 20)
rl.add([20, 20])
rl.print
# Should display: [1, 5) [10, 20)
rl.add([20, 21])
rl.print
# Should display: [1, 5) [10, 21)
rl.add([2, 4])
rl.print
# Should display: [1, 5) [10, 21)
rl.add([3, 8])
rl.print
# Should display: [1, 8) [10, 21)
rl.remove([10, 10])
rl.print
# Should display: [1, 8) [10, 21)
rl.remove([10, 11])
rl.print
# Should display: [1, 8) [11, 21)
rl.remove([15, 17])
rl.print
# Should display: [1, 8) [11, 15) [17, 21)
rl.remove([3, 19])
rl.print
# Should display: [1, 3) [19, 21)

puts '='*50
RangeList.new([1,8,10,26,50,90]).tap{|r|r.add([10,50])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.add([27,50])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.add([8,50])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.add([26,27])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.add([27,28])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.add([12,28])}.tap(&:print)

puts '='*50
RangeList.new([1,8,10,26,50,90]).tap{|r|r.remove([10,50])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.remove([11,50])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.remove([10,30])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.remove([11,30])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.remove([9,30])}.tap(&:print)
RangeList.new([1,8,10,26,50,90]).tap{|r|r.remove([9,11])}.tap(&:print)
