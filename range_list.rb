class RangeList
  attr_accessor :data
  attr_accessor :idx_start, :idx_term
  
  def initialize(init_data = [])
    @data = init_data
  end

  def add(range)
    puts "current: #{self.inspect}"
    start, term = range
    
    # invalid range 
    return print_dev_info(:add, range) if start >= term 
    
    # init data
    if @data.length == 0 
      print_dev_info(:add, range) 
      @data = range
      return
    end
    
    self.idx_start = find_index(start)
    self.idx_term  = find_index(term)
    print_dev_info(:add, range, idx_start, idx_term)
    
    @data.slice!(idx_start + 1..idx_term) if idx_start < idx_term
    if idx_start.odd? && idx_term.odd?
      @data.insert(idx_start + 1, start, term)
    elsif idx_start.even? && idx_term.odd?
      @data.insert(idx_start + 1, term)
    elsif idx_start.odd? && idx_term.even?
      @data.insert(idx_start + 1, start)
    elsif idx_start.even? && idx_term.even?
      nil 
    end

    # data fix
    if @data[idx_start] == start && @data[idx_start + 1] == start 
      @data.slice!(idx_start..idx_start + 1)
    end
  end

  def remove(range)
    puts "current: #{self.inspect}"
    start, term = range

    # invalid range
    return print_dev_info(:remove, range) if start >= term

    # blank data
    if @data.length == 0 
      print_dev_info(:remove, range)
      return
    end

    self.idx_start = find_index(start)
    self.idx_term  = find_index(term)
    print_dev_info(:remove, range, idx_start, idx_term)

    @data.slice!(idx_start + 1..idx_term) if idx_start < idx_term
    if idx_start.odd? && idx_term.odd?
      nil
    elsif idx_start.even? && idx_term.odd?
      @data.insert(idx_start + 1, start)
    elsif idx_start.odd? && idx_term.even?
      @data.insert(idx_start + 1, term)
    elsif idx_start.even? && idx_term.even?
      @data.insert(idx_start + 1, start, term)
    end
    
    # data fix
    if @data[idx_start] == start && @data[idx_start + 1] == start 
      @data.slice!(idx_start..idx_start + 1)
    end
  end

  # Find the first index that +greater than or equal to+ the val on existing array.
  def find_index(val, idx_start = 0, idx_term = @data.length - 1)
    return idx_start if idx_start == 0 && val < @data[idx_start]

    idx_mid   = (idx_term + idx_start) / 2
    mid       = @data[idx_mid]
    mid_right = @data[idx_mid + 1]

    if mid <= val && mid_right.nil?
      idx_mid
    elsif mid <= val && val < mid_right
      idx_mid
    elsif mid_right <= val 
      find_index(val, idx_mid + 1, idx_term)
    elsif val < mid
      find_index(val, idx_start, idx_mid)
    end
  end

  # :nocov:
  def print
    puts 'result:  ' + self.inspect
    puts '-' * 40
  end
  # :nocov:

  def to_s 
    (0...@data.length / 2).map do |idx| 
      [ '[', 
        @data[idx * 2    ].to_s, ', ', 
        @data[idx * 2 + 1].to_s, ')'
      ].join
    end.join(' ')
  end

  def inspect
    return 'nil' if @data.nil? || @data.length == 0

    (0...@data.length / 2).map do |idx| 
      [ '[', 
        @data[idx * 2    ].to_s.rjust(2, ' '), ',  ', 
        @data[idx * 2 + 1].to_s.rjust(2, ' '), ')'
      ].join
    end.join(' ')
  end
  
  # pretty print to console, just for dev
  #   A sample print could look like thie:
  #     current: [ 1,   8) [10,  26) [50,  90)
  #     add:     [          12,  28)
  #     index:2 3
  #     result:  [ 1,   8) [10,  28) [50,  90)
  #     ----------------------------------------
  def print_dev_info(type, range, idx_start = nil, idx_term = nil)
    start, term = range 

    if start >= term # invalid range 
      puts "#{"#{type}: ".ljust(9, ' ')}[#{start}, #{term})"
      return 
    end

    if @data.length == 0 # init data
      puts "#{"#{type}: ".ljust(9, ' ')}[ #{start},   #{term})"
      return
    end
     
    puts [
      "#{type}: ".ljust(9, ' '),
      '[', 
      ' ' * (idx_start * 5), start.to_s.rjust(2, ' '),
      ',',
      idx_term == idx_start ? term : '',
      idx_term != idx_start ? " #{' ' * ((idx_term - idx_start) * 5 - 4)}#{term.to_s.rjust(2, ' ')}" : '',
      ')'
    ].join
    
    puts "index:#{idx_start} #{idx_term}"
  end
end
