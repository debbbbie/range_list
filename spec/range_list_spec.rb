require 'simplecov'
SimpleCov.start

require File.expand_path("../../range_list.rb", __FILE__)

describe RangeList do
  before { @range_list = RangeList.new([1, 8, 10, 26, 50, 90]) }

  it 'init check' do 
    @range_list.to_s.should == '[1, 8) [10, 26) [50, 90)'
  end

  it '#find_index' do 
    @range_list.find_index(0).should == 0
    @range_list.find_index(1).should == 0
    @range_list.find_index(8).should == 1
    @range_list.find_index(10).should == 2
    @range_list.find_index(11).should == 2
    @range_list.find_index(26).should == 3
    @range_list.find_index(27).should == 3
    @range_list.find_index(50).should == 4
    @range_list.find_index(90).should == 5
    @range_list.find_index(95).should == 5
  end

  describe '#add' do
    describe 'special case' do
      it 'from blank data' do 
        range_list = RangeList.new
        range_list.add([10, 12])
        range_list.data.should == [10, 12]
        range_list.to_s.should == '[10, 12)'
      end

      it 'invalid range' do 
        range_list = RangeList.new([10, 12])
        range_list.add([12, 12])
        range_list.data.should == [10, 12]
        range_list.to_s.should == '[10, 12)'
      end

      it 'invalid range 2' do 
        range_list = RangeList.new([10, 12])
        range_list.add([13, 12])
        range_list.data.should == [10, 12]
        range_list.to_s.should == '[10, 12)'
      end
    end
    
    it 'even-even' do
      @range_list.add([10, 50])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 10, 90]
      @range_list.to_s.should == '[1, 8) [10, 90)'
    end

    it 'even-even2' do 
      @range_list.add([10, 25])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 10, 26, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 26) [50, 90)'
    end

    it 'odd-even' do 
      @range_list.add([27, 50])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 10, 26, 27, 90]
      @range_list.to_s.should == '[1, 8) [10, 26) [27, 90)'
    end

    it 'odd-even 2' do 
      @range_list.add([8, 50])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 90]
      @range_list.to_s.should == '[1, 90)'
    end

    it 'odd-odd' do 
      @range_list.add([26, 27])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 10, 27, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 27) [50, 90)'
    end

    it 'odd-odd 2' do 
      @range_list.add([27, 28])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 10, 26, 27, 28, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 26) [27, 28) [50, 90)'
    end

    it 'even-odd' do 
      @range_list.add([12, 28])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 10, 28, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 28) [50, 90)'
    end

    it 'even-odd2' do 
      @range_list.add([10, 28])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 10, 28, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 28) [50, 90)'
    end
  end
  
  describe '#remove' do 
    describe 'special case' do
      it 'from blank data' do 
        range_list = RangeList.new
        range_list.remove([10, 12])
        range_list.data.should == []
        range_list.to_s.should == ''
      end

      it 'invalid range' do 
        range_list = RangeList.new([10, 12])
        range_list.remove([12, 12])
        range_list.data.should == [10, 12]
        range_list.to_s.should == '[10, 12)'
      end
      
      it 'invalid range 2' do 
        range_list = RangeList.new([10, 12])
        range_list.remove([13, 12])
        range_list.data.should == [10, 12]
        range_list.to_s.should == '[10, 12)'
      end
    end    

    it 'even-even' do 
      @range_list.remove([10, 50])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 50, 90]
      @range_list.to_s.should == '[1, 8) [50, 90)'
    end

    it 'even-even2' do 
      @range_list.remove([11, 50])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 10, 11, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 11) [50, 90)'
    end

    it 'odd-even' do 
      @range_list.remove([9, 11])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 11, 26, 50, 90]
      @range_list.to_s.should == '[1, 8) [11, 26) [50, 90)'
    end

    it 'odd-even 2' do 
      @range_list.remove([9, 10])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_even
      @range_list.data.should == [1, 8, 10, 26, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 26) [50, 90)'
    end

    it 'odd-odd' do 
      @range_list.remove([9, 26])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 50, 90]
      @range_list.to_s.should == '[1, 8) [50, 90)'
    end
    
    it 'odd-odd 2' do 
      @range_list.remove([27, 28])
      @range_list.idx_start.should be_odd
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 10, 26, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 26) [50, 90)'
    end

    it 'even-odd' do 
      @range_list.remove([10, 30])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 50, 90]
      @range_list.to_s.should == '[1, 8) [50, 90)'
    end

    it 'even-odd2' do 
      @range_list.remove([11, 28])
      @range_list.idx_start.should be_even
      @range_list.idx_term.should be_odd
      @range_list.data.should == [1, 8, 10, 11, 50, 90]
      @range_list.to_s.should == '[1, 8) [10, 11) [50, 90)'
    end
  end
end
